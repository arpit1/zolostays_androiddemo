package com.arpit.zolostaysdemo.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.arpit.zolostaysdemo.R;

import java.util.ArrayList;

public class CircularView extends View {

    public static class MenuCircle {
        private int x, y;
        public int id;
        boolean status;
    }

    private Paint mainPaint;
    private Paint secondPaint;
    private int radius_main = getResources().getInteger(R.integer.radius_main);

    private int radialCircleRadius = getResources().getInteger(R.integer.radialCircleRadius);
    private ArrayList<MenuCircle> elements;

    public CircularView(Context context) {
        super(context);
        init();
    }

    public CircularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircularView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        elements = new ArrayList<>();
    }

    public void addMenuItem(boolean status, int id) {
        MenuCircle item = new MenuCircle();
        item.id = id;
        item.status = status;
        elements.add(item);
    }

    public void updateMenuItem(boolean status, int id) {
        MenuCircle item = new MenuCircle();
        item.id = id;
        if (id < 6)
            id = id + 11;
        else
            id = id - 6;
        item.status = status;
        elements.set(id, item);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mainPaint = new Paint();
        mainPaint.setColor(getResources().getColor(R.color.white));
        secondPaint = new Paint();
        secondPaint.setColor(getResources().getColor(R.color.small_circle));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int centerX = canvas.getWidth() / 2;
        int centerY = canvas.getHeight() / 2;
        Paint paint;
        for (int i = 0; i < elements.size(); i++) {
            double angle;
            double startAngle = -Math.PI / 2f;
            if (i == 0) {
                angle = startAngle;
            } else {
                angle = startAngle + (i * ((2 * Math.PI) / elements.size()));
            }
            int menuInnerPadding = 30;
            elements.get(i).x = (int) (centerX + Math.cos(angle) * (radius_main + menuInnerPadding + radialCircleRadius));
            elements.get(i).y = (int) (centerY + Math.sin(angle) * (radius_main + menuInnerPadding + radialCircleRadius));

            if (elements.get(i).status)
                paint = secondPaint;
            else
                paint = mainPaint;
            if (i <= 6) {
                if (i <= 3)
                    canvas.drawCircle(elements.get(i).x, elements.get(i).y, radialCircleRadius, paint);
                else
                    canvas.drawCircle(elements.get(i).x, elements.get(i).y - 10, radialCircleRadius, paint);
            } else if (i >= 12) {
                if (i < 15)
                    canvas.drawCircle(elements.get(i).x, elements.get(i).y + 10, radialCircleRadius, paint);
                else
                    canvas.drawCircle(elements.get(i).x, elements.get(i).y, radialCircleRadius, paint);
            }

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            for (MenuCircle mc : elements) {
//                double distance = Math.hypot(event.getX() - mc.x, event.getY() - mc.y);
//                if (distance <= radialCircleRadius) {
//                    return true;
//                }
//            }
//
//        }

        return super.onTouchEvent(event);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

    }
}
