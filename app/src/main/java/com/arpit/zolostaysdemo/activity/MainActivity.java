package com.arpit.zolostaysdemo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arpit.zolostaysdemo.R;
import com.arpit.zolostaysdemo.views.CircularView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    private int counter = 0;
    private static final int MAX_VAL = 12;

    @BindView(R.id.earn)
    TextView earn;

    @BindView(R.id.redeem)
    TextView redeem;

    @BindView(R.id.c_menu)
    CircularView circularView;

    @BindView(R.id.visits_count)
    TextView visitsCount;

    @BindView(R.id.max_count)
    TextView maxCount;

    @BindView(R.id.progress_wheel)
    ProgressBar seekBar;

    @BindView(R.id.semi_circle)
    ProgressBar semiCircle;

    private Unbinder unbinder;

    @BindView(R.id.heading_text)
    TextView headingText;

    @BindString(R.string.visit_earn)
    String headingString;

    @BindView(R.id.sub_heading_text)
    TextView subHeadingText;

    @BindString(R.string.every_time)
    String subHeadingString;

    @BindView(R.id.footer_text)
    TextView footerText;

    @BindString(R.string.gift_card)
    String footerString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder =  ButterKnife.bind(this);

        initialize();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initialize() {

        Toast.makeText(MainActivity.this, ""+getResources().getInteger(R.integer.yash), Toast.LENGTH_SHORT).show();
        headingText.setText(headingString);
        subHeadingText.setText(subHeadingString);
        footerText.setText(footerString);

        semiCircle.setProgress(MAX_VAL);   // Main Progress
        semiCircle.setSecondaryProgress(13); // Secondary Progress
        semiCircle.setMax(13); // Maximum Progress

//        Adding 20 items in the circular view
        for (int i = 1; i <= 17; i++) {
            if (i == 1 || i >= 13) {
                circularView.addMenuItem(true, i);
                counter++;
            } else
                circularView.addMenuItem(false, i);
        }

        seekBar.setProgress(counter);   // Main Progress
        seekBar.setSecondaryProgress(17); // Secondary Progress
        seekBar.setMax(17); // Maximum Progress

        if (seekBar != null) {
            seekBar.setClickable(false);
            seekBar.setFocusable(false);
            seekBar.setEnabled(false);
            semiCircle.setClickable(false);
            semiCircle.setFocusable(false);
            semiCircle.setEnabled(false);
        }
        setCount();
    }

    private void setListener() {
        maxCount.setText(String.valueOf(MAX_VAL));
        earn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (counter >= MAX_VAL) {
                    counter = 0;
                    for (int i = 1; i <= 17; i++)
                        circularView.updateMenuItem(false, i);
                }

                if (seekBar != null) {
                    counter++;
                    seekBar.setProgress(counter);
                    circularView.updateMenuItem(true, counter);
                }
//                Redrawing the Circular view after updating the status of the circle
                circularView.invalidate();
                setCount();
            }
        });
    }

    private void setCount() {
        visitsCount.setText(String.valueOf(counter));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
